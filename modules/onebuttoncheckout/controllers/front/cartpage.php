<?php 
protected function _getPaymentMethods()
	{
		if ($this->context->cart->OrderExists())
			return '<p class="warning">'.Tools::displayError('Error: This order has already been validated.').'</p>';
		
		if (count($this->context->cart->getDeliveryOptionList()) == 0 && !$this->context->cart->isVirtualCart())
		{
			if ($this->context->cart->isMultiAddressDelivery())
				return '<p class="warning">'.Tools::displayError('Error: None of your chosen carriers deliver to some of the addresses you have selected.').'</p>';
			else
				return '<p class="warning">'.Tools::displayError('Error: None of your chosen carriers deliver to the address you have selected.').'</p>';
		}
		if (!$this->context->cart->getDeliveryOption(null, false) && !$this->context->cart->isVirtualCart())
			return '<p class="warning">'.Tools::displayError('Error: Please choose a carrier.').'</p>';
		if (!$this->context->cart->id_currency)
			return '<p class="warning">'.Tools::displayError('Error: No currency has been selected.').'</p>';

		/* If some products have disappear */
		if (is_array($product = $this->context->cart->checkQuantities(true)))
			return '<p class="warning">'.sprintf(Tools::displayError('An item (%s) in your cart is no longer available in this quantity. You cannot proceed with your order until the quantity is adjusted.'), $product['name']).'</p>';

		if ((int)$id_product = $this->checkProductsAccess())
			return '<p class="warning">'.sprintf(Tools::displayError('An item in your cart is no longer available (%s). You cannot proceed with your order.'), Product::getProductName((int)$id_product)).'</p>';

		/* Check minimal amount */
		$currency = Currency::getCurrency((int)$this->context->cart->id_currency);

		$minimal_purchase = Tools::convertPrice((float)Configuration::get('PS_PURCHASE_MINIMUM'), $currency);
		if ($this->context->cart->getOrderTotal(false, Cart::ONLY_PRODUCTS) < $minimal_purchase)
			return '<p class="warning">'.sprintf(
				Tools::displayError('A minimum purchase total of %1s (tax excl.) is required to validate your order, current purchase total is %2s (tax excl.).'),
				Tools::displayPrice($minimal_purchase, $currency), Tools::displayPrice($this->context->cart->getOrderTotal(false, Cart::ONLY_PRODUCTS), $currency)
			).'</p>';

		/* Bypass payment step if total is 0 */
		if ($this->context->cart->getOrderTotal() <= 0)
			return '<p class="center"><button class="button btn btn-default button-medium" name="confirmOrder" id="confirmOrder" onclick="confirmFreeOrder();" type="submit"> <span>'.Tools::displayError('I confirm my order.').'</span></button></p>';
		
		//tatar get payment
		$arr_payments = array();
		$set_payments = '';
		$hdisplaypayments = Hook::exec('displayPayment', array(), null ,true);

			




		$kl = 0;
		foreach ($hdisplaypayments as $k => $hdisplaypayment){

			preg_match_all('/<p[^>]*?class="(.*?)".*?>(.*?)<\/p>/ms', $hdisplaypayment, $matches, 1);
			
			preg_match_all('/<form[^>]*?id="(.*?)".*?>(.*?)<\/form>/ms', $hdisplaypayment, $matches_form, 1);
			
			//-----------start of viabill code-------------------
			preg_match_all('/<div[^>]*?class="(.*?)".*?>(.*?)<\/div>/ms', $hdisplaypayment, $matches_div, 1);
			//-----------end of viabill code--------------------
			

			
			foreach ($matches[0] as $h => $matche){
				
				if($matches_form[1]){
					
					$set_payments .= '<div class="vsi_payment vsi_paywith_'.$matches_form[1][$h].'">
						<input type="radio" class="pay_radio" name="pay_radio_v" value="'.$matches_form[1][$h].'" '.($this->context->cookie->select_payment == $matches_form[1][$h]?'checked="checked"':'').'>
						<span class="pay_radio_op"></span>
						'.$matches_form[0][$h].$matche.'
					</div>';
				}
				
				else {
					//$arr_payments[$kl]['payment_id'] = $k;
					//$arr_payments[$kl]['payment_content'] = $matche;
					$set_payments .= '<div class="vsi_payment vsi_paywith_'.$k.'">
						<input type="radio" class="pay_radio" name="pay_radio_v" value="'.$k.'" '.($this->context->cookie->select_payment == $k?'checked="checked"':'').'>
						<span class="pay_radio_op"></span>
						'.$matche.'
					</div>';
				}
				$kl++;
			}

			//---------------------start of viabill code ----------------------------------------------------
			 $pricetagsrc = Configuration::get('VIABILL_EPAY_PRICETAG');
			 if(!empty($pricetagsrc)){
			 		$showpricetag == 'TRUE';
			 		$ordertotalvb = $this->context->cart->getOrderTotal();
					$this->context->smarty->assign('vbpricetagsrc', $pricetagsrc);
					$this->context->smarty->assign('ordertotalvb', $ordertotalvb);
			 }
			
			

				
				foreach($matches_div[0] as $p => $matchevb){
						if($matches_form[1]){
							//print_r($matchevb);

								//echo "hello";
							$set_payments .= '<div class="vsi_payment vsi_paywith_'.$matches_form[1][$p].'">
						<input type="radio" class="pay_radio" name="pay_radio_v" value="'.$matches_form[1][$p].'" '.($this->context->cookie->select_payment == $matches_form[1][$p]?'checked="checked"':'').'>
						
						'.$matches_form[0][$p].$matchevb.'
					</div> <script type="text/javascript">'.$pricetagsrc.'  </script>';

						}			

				}// end of foreach for viabill
			//------------------------------end of viabill code-------------------------				
		}

			 //------------------------------start of viabill code----------------------

				if($pricetagsrc){
							
							$set_payments .= "<script>
							setTimeout(function(){								
								if(vb.isLow('$ordertotalvb')  || vb.isHigh('$ordertotalvb')){									
								}
								else{
															
									$('#defaulttext').html('<div id=vbpricetag style=line-height:29px> ViaBill køb nu - betal, når du vil</div>');
									
								}	
							}, 1000);
							</script>";
						}
		
 			//------------------------------end of viabill code----------------------

				
		$this->context->smarty->assign('select_payment', $this->context->cookie->select_payment);
		//$this->context->smarty->assign('set_payments', $set_payments);
		return $set_payments;
	}
?>